<?php

include_once("file.php");

if (!class_exists("picture")) {
    class picture extends file
    {

        /**
         * @method get_first_picture_recursif(string | $folder)
         * @return string|$imagePath
         */
        function get_first_picture_recursif($folder)
        {
            $list = array_reverse($this->get_dir($folder));
            foreach ($list as $scan) {
                $type = $this->get_type($folder . "/" . $scan);
                if ($type == "folder") {
                    return $this->get_first_picture_recursif($folder . "/" . $scan . "/");
                } else if ($type == "image") {
                    return $folder . "/" . $scan;
                }
            }
        }

        /**
         * @method resize($source, $cible, $attribut, $max)
         * @return string|$cible
         */
        function resize($source, $cible, $attribut, $max)
        {
            if (function_exists("gd_info") == false) {
                return $this->error("La librairie GD n'est pas disponible");
            }
            if (function_exists("imagecreatefrompng") == false) {
                return $this->error("La librairie GD n'est pas disponible pour PNG");
            }
            if (!$this->exists($source)) {
                "source inexistante";
            }
            $ext = $this->get_extension($source);
            $sizeSource = getimagesize($source);
            $quality = 70;
            switch (strtolower($ext)) {
                case "jpg":
                case "jpeg":
                    $imageCible = $this->calculresize(imagecreatefromjpeg($source), $attribut, $max, $sizeSource);
                    $imageRedim = imagejpeg($imageCible, $cible, $quality);
                    break;
                case "png":
                    $imageCible = $this->calculresize(imagecreatefrompng($source), $attribut, $max, $sizeSource);
                    $imageRedim = imagepng($imageCible, $cible, round(($quality - 10) / 10));
                    break;
                case "gif":
                    $imageCible = $this->calculresize(imagecreatefromgif($source), $attribut, $max, $sizeSource);
                    $imageRedim = imagegif($imageCible, $cible);
                    break;
                default:
                    return $this->error("Non-support de l'extension d'image " . $source);
            }
            if ($imageRedim === false) {
                return $this->error("Probleme de redimensionnement d'image " . $ext . " pour " . $source);
            }
            imagedestroy($imageCible);
            return $cible;
        }

        function calculresize($imageSource, $attribut, $max, $sizeSource)
        {
            $src_w = $sizeSource[0];
            $src_h = $sizeSource[1];
            // Si l'argument attribut est un nombre
            if (is_numeric($attribut) && is_numeric($max)) {
                // Mode redimensionement + crop
                if ($attribut == $src_w && $max == $src_h) {
                    print("rien a faire sinon de copier la source sur la cible");
                    //copy($imageSource,$imageCible);
                    return true;
                } else {
                    // creer thumbnail avec 2 dimensions (int)largeur x (int)hauteur
                    $imageCible = imagecreatetruecolor($attribut, $max);
                    if (($src_w / $attribut) <= ($src_h / $max)) {
                        $temp = $src_w * ($max / $attribut);
                        imagecopyresampled($imageCible, $imageSource, 0, 0, 0, ($src_h - $temp) / 2, $attribut, $max, $src_w, $temp);
                    } else {
                        $temp = $src_h * ($attribut / $max);
                        imagecopyresampled($imageCible, $imageSource, 0, 0, ($src_w - $temp) / 2, 0, $attribut, $max, $temp, $src_h);
                    }
                }
            } else {
                // Mode redimensionement simple
                if (is_nan($max) && is_numeric($attribut)) {
                    $temp_var = $attribut;
                    $attribut = $max;
                    $max = $temp_var;
                }
                if ($attribut == "w" || ($attribut == "a" && $src_w > $src_h)) {
                    $dst_w = $max;
                    $dst_h = round(($dst_w / $src_w) * $src_h);
                } else if ($attribut == "h" || ($attribut == "a" && $src_w < $src_h)) {
                    $dst_h = $max;
                    $dst_w = round(($dst_h / $src_h) * $src_w);
                } else {
                    return $this->error("dimension non comprises " . $attribut . " x " . $max);
                }
                if ($dst_h <= $src_h && $dst_w <= $src_w) {
                    $imageCible = imagecreatetruecolor($dst_w, $dst_h);
                    imagecopyresampled($imageCible, $imageSource, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
                } else {
                    return $this->error("Image source (" . $src_w . "x" . $src_h . ") trop petite pour faire du " . $dst_w . "x" . $dst_h);
                }
            }
            return $imageCible;
        }
    }
}
