<?php
/**
 * Created by PhpStorm.
 * User: agoyffon
 * Date: 24.01.19
 * Time: 16:51
 */

class Security extends Mysql
{
    function ftp_authenticate() {
        header('WWW-Authenticate: Basic realm="Autentication FTP"');
        header('HTTP/1.0 401 Unauthorized');
        print("Vous devez entrer un identifiant FTP et un mot de passe valides pour accéder
	à cette ressource.");
        exit;
    }

    function mysql_authenticate(){
        $array_free_pages=array("compte_connexion.php");
        if(!isset($_SESSION["id_compte"]) && !in_array(basename($_SERVER["PHP_SELF"]),$array_free_pages) && !isset($_POST["password"])){
            // Plus de session active
            echo "Votre session de ".(ini_get("session.gc_maxlifetime")/60)." mn a expirée. Veuillez vous reconnecter, SVP.";
            header("location:compte_connexion.php");exit();
        }else{
            if(!in_array($_GET["page"],$array_free_pages)){
                // Data pour requetage SQL
                return $this->result_array("SELECT id AS id_compte,prenom,nom,email FROM album_compte WHERE id=".$_SESSION["id_compte"]);
            }
        }
    }
}
