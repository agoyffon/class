<?php
// Class autoload
function my_autoloader($class) {
    if(file_exists(__DIR__."/".$class . '.php')){
        include_once(__DIR__."/".$class . '.php');
    }
}
if(function_exists("spl_autoload_register")){
    spl_autoload_register('my_autoloader');
}else{
    include_once("mvc.php");
    include_once("file.php");
}
include_once("mvc.php");
$mvc = new mvc();

include_once("file.php");
$file = new file();

if(!headers_sent()){
    session_start();
}