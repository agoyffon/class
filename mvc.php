<?php
if (!class_exists(basename(__FILE__, ".php"))) {
    class mvc{
        var $constTemplate = "template";
        
        // Init
        function init($template, $array){
            if (is_object($array)){
                $array = $this->ObjectToArray($array);
            }
            $page = file_get_contents($template . ".htm");
            $page = $this->includer($page);
            $array = array_merge($_GET, $array, array($this->constTemplate => $template));
            $page = $this->shortcode_replace($page, $array);
            return ($page);
        }

        // replaceStringByArray
        function replaceStringByArray($string, $array){
            if (is_object($array)){
                $array = $this->ObjectToArray($array);
            }
            foreach ($array as $search => $replace) {
                $string = str_replace($search, $replace, $string);
            }
            return $string;
        }

        // Shortcode replace
        function shortcode_replace($string_page, $array){
            $result='';
            if (!is_string($string_page)) {
                return $this->error("<h3>shortcode_replace</h3> 1rst argument expecting type string but got type " . gettype($string_page));
            }elseif(empty($string_page) && isset($GET["debug"])){
                return $this->error("<h3>shortcode_replace</h3> 1rst argument expecting non-empty but got empty string");
            }elseif (is_array($array) && count($array) == 0 && isset($GET["debug"])) {
                return $this->error("<h3>shortcode_replace</h3> 2nd argument expecting populated but got empty array");
            }elseif (!is_array($array) && !is_object($array)) {
                return $this->error("<h3>shortcode_replace</h3> 2nd argument expecting array or object but got type " . gettype($array)." : ".$array);
            }
            
            if (is_object($array)){
                $array = $this->ObjectToArray($array);
            }
            if(is_string(current(array_keys($array)))){
                $result=$string_page;
                //echo "<hr>Array is associative ".print_r(array_keys($array,true));
                foreach ($array as $key => $val) {
                    //if ($key == "date") $val = $this->date_format_fr($val);
                    //if ($key == "date_notification") $val = substr($val, 0, 10);
                    if (isset($_GET["err"]) && $_GET["err"] == 1 && substr_count($string_page,"[$key]") == 0) {
                        return $this->error("<h3>shortcode_replace</h3>No [$key]");
                    }
                    if (is_string($val) || is_int($val) || is_double($val)) {
                        $result = str_replace("[$key]", stripslashes(trim($val)), $result);
                    }elseif (is_array($val)){
                        if(count($val)==0){
                            continue;
                        }
                        return $this->error("<h3>shortcode_replace</h3> 2nd argument expecting bi-dimensional array but got multi-dimensional array : ".print_r($val, true));
                    }elseif(is_object($val)) {
                        return $this->error("<h3>shortcode_replace</h3> 2nd argument expecting bi-dimensional array but got multi-dimensional object : ".print_r($val, true));
                    }elseif(isset($_GET["debug"])){
                        return $this->error("<h3>shortcode_replace</h3>[<b><i>$key</i></b>] string is expected to be int or string but got " . gettype($val) .":". print_r($array, true)."</pre>");
                    }
                }
            }else{
                foreach ($array as $key=>$val) {
                    if (isset($_GET["debug"]) && substr_count($string_page,"[val]") == 0) {
                        return $this->error("<h3>shortcode_replace</h3>No [val]");
                    }
                    if (is_string($val) || is_int($val) || is_double($val)) {
                        //print("replace [val] by $val in $string_page");
                        $result .= str_replace("[val]", stripslashes(trim($val)), $string_page);
                    }else{
                        return $this->error("<h3>shortcode_replace</h3>[<b><i>$key</i></b>] else is expected to be int or string but got " . gettype($val) .":". print_r($array, true)."</pre>");
                    }
                } 
            }
            return $result;
        }

        /* @method includer(string)
         * @usage [include:file]
         */
        function includer($page){
            preg_match_all("|\[include:([^\]]*)\]|",$page,$includes);
            if(isset($includes[1])){
                foreach($includes[1] as $file){
                    $curl=file_get_contents($file);
                    $page=str_replace("[include:".$file."]",$curl,$page);
                }
            }
            return $page;
        }

        function optimzedHtml($code,$continue=true){
            if(!headers_sent())header("Pragma:private");	//Cache utilisateur
            if($continue===true){
                //remplace les espaces vides entre les balises html
                $code=preg_replace("/>[\t\r\n ]*</" ,"><" , $code);
                //compression css
                $code=preg_replace("/([{};])[\t\r\n ]*/" ,"$1" , $code);
                //compression javascript
                $code=preg_replace("/([;{])[\t\r\n ]*([\$}ivrw])/" ,"$1 $2" , $code);
            }
            return $code;
        }

        //Box
        function box($msg = "", $colorText = "#333333", $colorBg = "#DDDDDD"){
            if (is_array($msg)) $msg = print_r($msg, true);
            return "<div style='padding:20px; margin:20px 0;background-color:" . $colorBg . ";color:" . $colorText . ";'>" . $msg . "</div>";
        }

        // Error
        function error($msg = "", $return = false){
            $msg = $this->box($msg, "red", "#FFEEEE");
            if ($return === true) {
                return ($msg);
            } else {
                print($msg);
            }
            return false;
        }

        // Warning
        function warning($msg = "", $return = false){
            $msg = $this->box($msg, "orange", "#ffDD88");
            if ($return === true) {
                return ($msg);
            } else {
                print($msg);
            }
            return false;
        }

        // Info
        function info($msg = "", $return = false){
            $msg = $this->box($msg, "green", "#EEFFEE");
            if ($return === true) {
                return ($msg);
            } else {
                print($msg);
            }
            return false;
        }

        //function get_dir_with_MVC($option=array())
        function arrayToList($option = array()){
            $optionByDefault = array(
                "array" => array(),
                "reverse" => false,
                "size" => false,
                "target" => "",
                "template" => "<li><a class='item' href='?id=[val]'>[val]</a></li>",
                "limit" => 0,
                "selected" => ""
            );
            if(!isset($option["array"]) && is_array($option)){
                //throw new ErrorException("give an array or die");
            }
            foreach ($optionByDefault as $key => $value) {
                if (!isset($option[$key])) {
                    $option[$key] = $value;
                }
            }
            if ($option["reverse"] === true) $option["array"] = array_reverse($option["array"]);
            if ($option["limit"] > 0) $option["array"] = array_slice($option["array"], 0, $option["limit"]);
            $output = "";
            if(is_array(current($option["array"]))){
                foreach ($option["array"] as $key => $val) {
                    $output .= $this->shortcode_replace($option["template"],$val);
                }
            }else{
                if(!empty($option['selected'])){
                    //$localArray = array("key"=>$key,"val" => $val, "selected" => "");
                }else{
                    $output = $this->shortcode_replace($option["template"],$option["array"]);
                }
            }
            return $output;
        }

        function human_filesize($file, $decimals = 2){
            if (!file_exists($file)) return $this->error("Fichier inexistant " . $file);
            $bytes = filesize($file);
            $sz = 'BKMGTP';
            $factor = (int)round((strlen($bytes) - 1) / 3);
            return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
        }

        function date_format_fr($str){
            if (strlen($str>"10") === false) {
                return $str;
            } else {
                $date = explode("-", $str);
                if (strpos($date[2], " ") !== false) {
                    $jour_plus_heure = explode(" ", $date[2]);
                    $hms = "";
                    if ($_GET["page"] == "compte_notifications" || $_GET["page"] == "projet_encours") {
                        $hms = explode(":", $jour_plus_heure[1]);
                        $hms = "à " . $hms[0] . ":" . $hms[1];
                    }
                    return $jour_plus_heure[0] . "/" . $date[1] . $hms;
                } else {
                    return $date[2] . "/" . $date[1] . "/" . $date[0];
                }
            }
        }

        function date_format_unixToFr($unix){
            if (!is_int($unix)) {
                return $this->error($unix . " is not a valid UNIX timestamp");
            } else {
                $date = new DateTime();
                $date->setTimestamp($unix);
                return $date->format('m/Y');
            }
        }

        function date_format_de_en($dateDe){
            $month = substr($dateDe, 3, 2);
            $day = substr($dateDe, 0, 2);
            $year = substr($dateDe, 6, 4);
            return $year."-".$month."-".$day;
        }

        function date_format_revolut($date){
            $mois_array=array(
                "janv."=>"01",
                "fev."=>"02",
                " mars "=>"03",
                " avr. "=>"04",
                " mai "=>"05",
                " juin "=>"06",
                " juil "=>"07",
                " août "=>"08",
                " sept "=>"09",
                " oct ."=>"10",
                " nov. "=>"11",
                " déc. "=>"12",
                " Jan "=>"01",
                " Feb "=>"02",
                " Mar "=>"03",
                " Apr. "=>"04",
                " May "=>"05",
                " Jun "=>"06",
                " Jul "=>"07",
                " Aug "=>"08",
                " Sep "=>"09",
                " Oct "=>"10",
                " Nov "=>"11",
                " Dec "=>"12"
            );
            /*
            foreach($mois_array as $k=>$v){
                if(strpos($date,$k)!==false){
                    $month=$v;
                }
            }*/
            $day = str_replace(" ","",substr($date, 0, 2));
            if(strlen($day)==1){
                $day="0".$day;
                $month = $this->replaceStringByArray(substr($date,1,-4),$mois_array);
            }else{
                $month = $this->replaceStringByArray(substr($date,2,-4),$mois_array);
            }
            $year = substr($date, -4);
            //$month = $this->replaceStringByArray(substr($month,0,6),$mois)));
            return $year."-".$month."-".$day;
        }
        function date_format_en($dateDe){
            $month = substr($dateDe, 5, 2);
            $day = substr($dateDe, 8, 2);
            $year = substr($dateDe, 0, 4);
            return $year."-".$month."-".$day;
        }

        function date_format_fr_to_en($dateFr){
            $month = substr($dateFr, 3, 2);
            $day = substr($dateFr, 0, 2);
            $year = substr($dateFr, 6, 4);
            return $year."-".$month."-".$day;
        }

        function objectToArray($object){
            $array = array();
            foreach ($object as $key => $value) {
                $array[$key] = $value;
            }
            return $array;
        }

        function object_merge($objects){
            $mergedObject = new stdClass();
            foreach ($objects as $object) {
                if(!is_array($object) && !is_object($object)){
                    //
                }else{
                    foreach ($object as $key => $val) {
                        $mergedObject->$key = $val;
                    }
                }
            }
            return $mergedObject;
        }

        function redirect($page){
            header("location:".$page);
        }
    }
}
