<?php
/**
 * @method ini(string|$templateURL)
 */
include_once ("mvc.php");

if(!class_exists(basename(__FILE__,".php"))){
	class File extends mvc {

        var $constFolder = "folder";

		function get_extension($filename,$keepCase=false){
            $point=strrpos($filename,".")+1;
            $ext=substr($filename,$point);
			if(!$keepCase){
                $ext=strtolower($ext);
            }
            return $ext;
		}

		function get_type($url){
            if($this->exists($url) === false){
                $this->error($url." not found");
            }
            if(is_dir($url)){
                return $this->constFolder;
            }
            $ext=$this->get_extension($url);
            $array_file=array(
                "image"=>array("png","jpg","jpeg","gif"),
                "video"=>array("mp4","webm","mov"),
                "html" =>array("html","htm"),
                "svg" =>array("svg")
            );
            foreach ($array_file as $type=>$extensions){
                if(in_array($ext,$extensions)) {
                    return $type;
                }
            }
            return $ext;
		}

		function filterFolderChildrenByType($folder,$type){
            $arrayReturn = array();
            foreach($this->get_dir($folder) as $filename){
                if($this->get_type($folder."/".$filename) == $type){
                    $arrayReturn[] = $filename;
                }
            }
            return($arrayReturn);
        }

        /*
	     * @method printGzip(string)
	     */
        function printGzip($code){
	        if(headers_sent() === true){
	        	$this->error("Could not gzip compress. Headers sent");
		        print($code);
	        }else{
		        header('Content-Length:'.strlen($code));
		        header('Content-Encoding:gzip');
		        ob_start('ob_gzhandler');
		        print($code);
		        ob_end_flush();
	        }

        }

        /* @method get_dir(string $folder, array $remove)
         * @return array with sub files
         * @see scandir
         * @
         */
		function get_dir($folder, $remove=array()){
            if(!is_array($remove)){
                $remove=array($remove);
            }
            $remove=array_merge($remove,array('..', '.'));
            if(!is_readable($folder)){
                $this->error("Permission denied");
            }
            if($this->get_type($folder) !== $this->constFolder){
                $this->error("Not a folder but a ".$this->get_type($folder));
            }
            return array_diff(scandir($folder),$remove);
		}

        function get_next_dir($root, $folder){
		    $found = false;
            $parentArray = $this->get_dir($root);
            foreach ($parentArray as $key => $value){
                if($found){
                    $next_parent = $parentArray[$key];
                    if($this->get_type($root."/".$next_parent)==$this->constFolder) {
                        return $next_parent;
                    }
                }elseif($value == $folder) {
                    $found = true;
                }
            }
        }

        function get_content ($url){
            if(!file_exists($url)){
				$this->error($url." n'existe pas (get_content) !");
				return false;
			}
            return file_get_contents($url);
        }

        function exists($url,$alert=true){
		    if(!is_string($url)){
                return $this->error(print_r($url,true)." n'est pas un chemin de type 'string' pour ".__METHOD__."() dans ".__FILE__.":".__LINE__);
            }else if(file_exists($url)===false){
		        if($alert){
                    return $this->error($url." n'existe pas pour pour ".__METHOD__."() dans ".__FILE__.":".__LINE__);
                }else{
		            return false;
                }
            }
            return true;
        }

        function loadJSON($url){
            $file_content = $this->get_content($url);
            $json = json_decode($file_content);
            if(!is_array($json) && !is_object($json)){
                $this->error("Invalid JSON ".$url."<hr>".$file_content );
            }
            return $json;
        }

		function getMiniature($miniature_dir,$path,$file){
			if($path!=""){
				return($miniature_dir.$file);
            }else{
				return($miniature_dir.str_replace("/","_",$path).".".$file);
			}
		}

        function getMiniaturePath($path){
			$returnPath = str_replace("source/","/", $path);
			return("miniature/".str_replace("/","_",$returnPath));
        }

		function getHTMLViewFromArray($model,$array){
			$output="";
			foreach($array as $element){
			    $output.=$this->shortcode_replace($model,$element);
			}
			return($output);
		}
	}
}
