<?php
if(!class_exists("mvc")){
    include_once("mvc.php");
}
if(!class_exists("mysql")){
class mysql extends mvc{
    var $msg;
    var $mysqli;
    var $conf;
    var $connected;

    //Connexion
    function __construct($conf,$debug){
        $this->mysqli = @new mysqli($conf["db_server"],$conf["db_user"],$conf["db_pass"],$conf["db_name"]);
        if($this->mysqli->connect_error!=""){
            if($debug==true){
                $this->error($this->mysqli->connect_error);
            }
            $this->mysqli=false;
            $this->connected=false;
        }else{
            $this->connected=true;
        }
    }

    //Requetage
    function query($requete,$debug=false) {
        if($this->mysqli){
           if($debug){
               print($this->info($requete));
               if(count($this->mysqli->error_list)){
                   $this->error($this->mysqli->error_list);
               }
               return false;
           }else{
                $result = $this->mysqli->query($requete);
                return $result;
           }
        }
    }

    //Analyse
    function fetch($resultat) {
        if($this->mysqli && $resultat->num_rows){
            $res=$resultat->fetch_assoc();
            if(is_array($res)){
                return array_map('utf8_encode',$res);
            }
            return $res;
        }
        return false;
    }

    //Proprieté de la table
    function table_properties($table,$debug=false){
        return $this->result_array("SHOW FULL COLUMNS FROM `".$table."`");
    }

    //Retourne les X premiers champs trouvé ayant le type $type
    function get_fields($table_properties,$type=null,$maxField=1){
        $return=array();
        foreach($table_properties as $field){
            $current_type=$this->parse_type($field["Type"]);
            if($type !== null && $current_type==$type){
                $maxField--;
            }
            if($maxField == 0){
                return $return;
            }
            $return[]=$field["Field"];
        }
        return $return;
    }

    function get_primary_key($table_properties){
        foreach($table_properties as $val){
            if($val["Key"]=="PRI"){
                break;
            }
        }
        return $val["Field"];
    }

    function parse_type($type){
        $pos=strpos($type,"(");
        if($pos){
            return substr($type,0,$pos);
        }
        return $type;
    }

    function parse_options($type){
        if($pos1=strpos($type,"'")){
            $parse=substr($type,$pos1+1,-2);
            return explode("','",str_replace("''","'",$parse));
        }else if($pos1=strpos($type,"(")){
            return substr($type,$pos1+1,-1);
        }
    }

    function list_tables(){
        if(!isset($_SESSION["db_name"])){
            $this->error("No db_name defined in mysql.conf");
        }
        $result=$this->result_array("SHOW TABLES FROM `".$_SESSION["db_name"]."`");
        foreach($result as $table){
            $tables[$table]=$this->sql("SELECT COUNT(*) FROM ".$table);
        }
        return $tables;
    }

    function table_exists($table){
        return array_key_exists($table,$this->list_tables());
    }

    //Requetage d'un champs unique (retourne le premier champs du premier résultat)
    function sql($requete,$debug=false){
        $res=$this->fetch($this->query($requete,$debug));
        if(is_array($res)){
            return current($res);
        }
        return $res;
    }

    //Requete complete
    function result_array($requete,$debug=false,$reduceArray=true){
        if(is_string($requete)){
            $requete=$this->query($requete,$debug);
        }
        $result_array=array();
        while($res=$this->fetch($requete)){
            if(count($res) == 1 && $reduceArray==true){
                $result_array[]=current($res);
            }else{
                $result_array[]=$res;
            }
        }
        return $result_array;
    }

    //Requete complete
    function result_array_object($request, $debug = false)
    {
        if (is_string($request)) $request = $this->query($request, $debug);
        $result_array = array();
        while ($res = $request->fetch_object()) {
            $result_array[] = $res;
        }
        return $result_array;
    }

    function table_status($table){
        $req_clef=$this->query("SHOW TABLE STATUS");
        while($res_clef=$this->fetch($req_clef)){
            if(isset($res_clef["Name"]) && $res_clef["Name"]==$table){
                return $res_clef;
            }
        }
    }

    //Data correction
    function data_correction($string){
        if(is_string($string)){
            $string=str_replace("’","'",$string);
            $string="'".utf8_decode(addslashes($string))."'";
        }
        return $string;
    }

    //MISE A JOUR
    function update($table,$data,$debug=false){
        $table_properties=$this->table_properties($table);
        $primary_key=$this->get_primary_key($table_properties);

        //mise à jour des champs libres
        if(isset($data[$primary_key])){
            $q="UPDATE `".$table."` SET ";
            foreach($table_properties as $val){
                // si le champs courrant de la base correspond à un data envoyée
                if(isset($data[$val["Field"]]) && $val["Field"]!=$primary_key){
                    $q.="`".$val["Field"]."`=".$this->data_correction($data[$val["Field"]]).", ";
                }
            }
            //supression de la dernière virgule et espace
            $q=substr($q,0,-2);
            $q.=" WHERE `".$primary_key."`=".$this->data_correction($data[$primary_key]);
            return $this->query($q,$debug);
        }else{
            $box_style="padding:20px; margin:20px;background-color:";
            print("<div style='".$box_style."#FFEEEE;'>Mise à jour impossible (".$table.") sans envoi de la clef primaire (".$primary_key.")</div>");
            return false;
        }
    }
	function update_redirect($table,$data,$redirect){
    	if($this->update($table,$data)){
    		header("location:".$redirect);
	    }
	}

    function insert($table,$data,$debug=false){
        $primary_key=$this->get_primary_key($this->table_properties($table));
        $sql="INSERT INTO `".$table."` (`".$primary_key."`) VALUES (NULL);";
        if($this->query($sql)){
            $data[$primary_key]=$this->insert_id();
            if($data[$primary_key]!==false){
                $this->update($table,$data,$debug);
                return $data[$primary_key];
            }
        }
        return false;
    }

    function import($table,$file,$method="csv")	{
        if(!file_exists($file)){
            $this->msg.="fichier '".$file."' inexistant  pour la table '".$table."'";
            return false;
        }
        $req="LOAD DATA LOCAL INFILE '".$file."' INTO TABLE `".$table."` FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'";
        $result=$this->query($req); //pré-test
        if($result){
            $this->query("TRUNCATE TABLE `".$table."`");
            $this->query($req);
        }
        return $result;
    }

    //Renvoie un résumé du texte
    function resume($texte="",$spacepos="150"){
        // recupère seulement le contenu texte (mais supprime le formatage du texte)
        $texte=strip_tags($texte);
        if(strlen($texte)>$spacepos){
            $spacepos=strpos($texte," ",$spacepos);
            if($spacepos>0){
                $texte=substr($texte,0,$spacepos)."...";
            }
        }
        return $texte;
    }

    // FONCTION  à faire hériter
    function insert_id(){
        return $this->mysqli->insert_id;
    }
    function num_rows($resultat){
        return $resultat->num_rows;
    }
    function last_error(){
        return $this->mysqli->error;
    }
}
}